extends KinematicBody2D

const speed_max = 20
const gravity = 50
var velocity = Vector2(0,0)

func get_movement():
	var movement = Vector2(0,0)
	
	if Input.is_action_pressed("ui_right"):
		movement.x = 1
	elif Input.is_action_pressed("ui_left"):
		movement.x = -1
	
	return movement.normalized()*speed_max

func _physics_process(delta):
	velocity = velocity + Vector2(0, gravity) + get_movement()
	velocity = move_and_slide(velocity)
	velocity = velocity*0.9
